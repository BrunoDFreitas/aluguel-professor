<?php

/**
 * FuncionarioIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class FuncionarioIdentity extends CUserIdentity
{
	/**
	 * Authenticates a user.
	 * The example implementation makes sure if the username and password
	 * are both 'demo'.
	 * In practical applications, this should be changed to authenticate
	 * against some persistent user identity storage (e.g. database).
	 * @return boolean whether authentication succeeds.
	 */
	public function authenticate()
	{
		$funcionario = Funcionario::model()->findByAttributes(array('UsuarioFuncionario'=>$this->username));
		
		if($funcionario === null)
			$this->errorCode=self::ERROR_USERNAME_INVALID;
		else 
		{
			if($funcionario->SenhaFuncionario !== md5($this->password))
				$this->errorCode=self::ERROR_PASSWORD_INVALID;
			else
			{
				$this->setState('CodFuncionario', $funcionario->CodFuncionario);
				$this->setState('NomeFuncionario', $funcionario->NomeFuncionario);
				$this->setState('IndicadorFuncionario', 'S');
				
				if($funcionario->IndicadorGerente == 'S')
					$this->setState('IndicadorGerente', $funcionario->IndicadorGerente);
				
				$this->errorCode=self::ERROR_NONE;
			}
		}
		return !$this->errorCode;
	}
}