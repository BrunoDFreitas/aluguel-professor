<?php

/**
 * This is the model class for table "funcionario".
 *
 * The followings are the available columns in table 'funcionario':
 * @property integer $CodFuncionario
 * @property string $UsuarioFuncionario
 * @property string $NomeFuncionario
 * @property string $SenhaFuncionario
 * @property string $IndicadorGerente
 */
class Funcionario extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'funcionario';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('UsuarioFuncionario, NomeFuncionario, SenhaFuncionario, IndicadorGerente', 'required'),
			array('UsuarioFuncionario', 'length', 'max'=>20),
			array('NomeFuncionario', 'length', 'max'=>50),
			array('SenhaFuncionario', 'length', 'max'=>32),
			array('IndicadorGerente', 'length', 'max'=>1),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('CodFuncionario, UsuarioFuncionario, NomeFuncionario, SenhaFuncionario, IndicadorGerente', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'CodFuncionario' => 'Cod Funcionario',
			'UsuarioFuncionario' => 'Usuario Funcionario',
			'NomeFuncionario' => 'Nome Funcionario',
			'SenhaFuncionario' => 'Senha Funcionario',
			'IndicadorGerente' => 'Indicador Gerente',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('CodFuncionario',$this->CodFuncionario);
		$criteria->compare('UsuarioFuncionario',$this->UsuarioFuncionario,true);
		$criteria->compare('NomeFuncionario',$this->NomeFuncionario,true);
		$criteria->compare('SenhaFuncionario',$this->SenhaFuncionario,true);
		$criteria->compare('IndicadorGerente',$this->IndicadorGerente,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Funcionario the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
