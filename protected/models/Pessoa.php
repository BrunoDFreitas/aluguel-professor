<?php

/**
 * This is the model class for table "pessoa".
 *
 * The followings are the available columns in table 'pessoa':
 * @property integer $CodPessoa
 * @property string $NomePessoa
 * @property string $CPFPessoa
 * @property string $EmailPessoa
 * @property string $GeneroPessoa
 * @property integer $CodEnderecoPessoa
 * @property string $TelefonePessoa
 * @property string $DataNascimentoPessoa
 * @property integer $EscolaridadePessoa
 * @property string $SenhaPessoa
 * @property string $SaldoPessoa
 * @property string $IndicadorProfessor
 * @property string $IndicadorExcluido
 */
class Pessoa extends CActiveRecord
{
	public $NomeDisciplina;
	public $CodDisciplina;
	public $AssuntoDisciplina;
	public $PrecoDisciplina;
	public $CodAula;
	public $PrecoAula;
	public $DataAula;
	

	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'Disciplina' => array(self::HAS_MANY, 'Disciplina', 'CodPessoa'),
			'Endereco' => array(self::HAS_ONE, 'Endereco', array('CodEndereco'=>'CodEnderecoPessoa')),
			'Escolaridade'=>array(self::HAS_ONE, 'Escolaridade', array('CodEscolaridade'=>'EscolaridadePessoa')),
		);
	}
	
	
	public function buscaProfessores($filtros=array())
	{
		$disciplina = '';
		$precoMin = 0.00;
		$precoMax = 999.99;
		
		if(isset($filtros['disciplina']))
		{
			$disciplina = $filtros['disciplina'];
		}
		
		if(isset($filtros['precoMin']))
		{
			$precoMin = $filtros['precoMin'];
		}
		
		if(isset($filtros['precoMax']))
		{
			$precoMax = $filtros['precoMax'];
		}
		
		$criteria = new CDbCriteria;
		$criteria->select = array('t.CodPessoa','t.NomePessoa', 'D.NomeDisciplina', 'D.CodDisciplina', 'D.AssuntoDisciplina', 'D.PrecoDisciplina');
		$criteria->join  = " LEFT JOIN disciplina D ON (t.CodPessoa = D.CodPessoa AND D.IndicadorExcluido = 'N') ";
		$criteria->addCondition("t.IndicadorExcluido = 'N'");
		$criteria->addCondition("t.IndicadorProfessor = 'S'");
		$criteria->addCondition("D.NomeDisciplina LIKE '%".$disciplina."%'");
		$criteria->addCondition("D.PrecoDisciplina BETWEEN ".$precoMin." AND ".$precoMax);
		$criteria->order = 't.NomePessoa';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public function buscaClientes($filtros=array())
	{
		$nome = '';
		
		if(isset($filtros['nome']))
		{
			$nome = $filtros['nome'];
		}
		
		$criteria = new CDbCriteria;
		$criteria->select = array('t.CodPessoa','t.NomePessoa', "CASE WHEN t.indicadorProfessor = 'S' THEN 'Professor' ELSE 'Aluno' END AS IndicadorProfessor");
		
		$criteria->addCondition("t.NomePessoa LIKE '%".$nome."%'");
		
		$criteria->order = 't.NomePessoa';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	
	public function buscaAulasPendentes()
	{
		$criteria = new CDbCriteria;
		if($this->IndicadorProfessor == 'S')
				$criteria->select = array('t.NomePessoa', 'D.NomeDisciplina', 'A.CodAula', 'A.PrecoAula', 'DATE_FORMAT(A.DataAula,"%d/%m/%Y") AS DataAula');
		else
			$criteria->select = array('P.NomePessoa', 'D.NomeDisciplina', 'A.CodAula', 'A.PrecoAula', 'DATE_FORMAT(A.DataAula,"%d/%m/%Y") AS DataAula');
		
		if($this->IndicadorProfessor == 'S')
			$criteria->join  = "INNER JOIN aula A ON (A.CodPessoaAluno = t.CodPessoa) INNER JOIN disciplina D ON (A.CodDisciplina = D.CodDisciplina)";
		else
			$criteria->join  = "INNER JOIN aula A ON (A.CodPessoaAluno = t.CodPessoa) INNER JOIN disciplina D ON (A.CodDisciplina = D.CodDisciplina) INNER JOIN pessoa P ON (P.CodPessoa = D.CodPessoa)";
		
		$criteria->addCondition("A.IndicadorExcluido = 'N'");
		$criteria->addCondition("A.IndicadorAulaAceiteProfessor = 'P'");
		if($this->IndicadorProfessor == 'S')
			$criteria->addCondition("D.CodPessoa = ".$this->CodPessoa."");
		else
			$criteria->addCondition("A.CodPessoaAluno = ".$this->CodPessoa."");
		$criteria->order = 't.NomePessoa';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pessoa';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('NomePessoa, CPFPessoa, EmailPessoa, GeneroPessoa, CodEnderecoPessoa, TelefonePessoa, DataNascimentoPessoa, SenhaPessoa, SaldoPessoa, IndicadorProfessor, IndicadorExcluido', 'required'),
			array('CodEnderecoPessoa, EscolaridadePessoa', 'numerical', 'integerOnly'=>true),
			array('NomePessoa, EmailPessoa', 'length', 'max'=>50),
			array('CPFPessoa', 'length', 'max'=>11),
			array('GeneroPessoa, IndicadorProfessor, IndicadorExcluido', 'length', 'max'=>1),
			array('TelefonePessoa', 'length', 'max'=>15),
			array('SenhaPessoa', 'length', 'max'=>32),
			array('SaldoPessoa', 'length', 'max'=>10),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('CodPessoa, NomePessoa, CPFPessoa, EmailPessoa, GeneroPessoa, CodEnderecoPessoa, TelefonePessoa, DataNascimentoPessoa, EscolaridadePessoa, SenhaPessoa, SaldoPessoa, IndicadorProfessor, IndicadorExcluido', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'CodPessoa' => 'Cod Pessoa',
			'NomePessoa' => 'Nome Pessoa',
			'CPFPessoa' => 'Cpfpessoa',
			'EmailPessoa' => 'Email Pessoa',
			'GeneroPessoa' => 'Genero Pessoa',
			'CodEnderecoPessoa' => 'Cod Endereco Pessoa',
			'TelefonePessoa' => 'Telefone Pessoa',
			'DataNascimentoPessoa' => 'Data Nascimento Pessoa',
			'EscolaridadePessoa' => 'Escolaridade Pessoa',
			'SenhaPessoa' => 'Senha Pessoa',
			'SaldoPessoa' => 'Saldo Pessoa',
			'IndicadorProfessor' => 'Indicador Professor',
			'IndicadorExcluido' => 'Indicador Excluido',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('CodPessoa',$this->CodPessoa);
		$criteria->compare('NomePessoa',$this->NomePessoa,true);
		$criteria->compare('CPFPessoa',$this->CPFPessoa,true);
		$criteria->compare('EmailPessoa',$this->EmailPessoa,true);
		$criteria->compare('GeneroPessoa',$this->GeneroPessoa,true);
		$criteria->compare('CodEnderecoPessoa',$this->CodEnderecoPessoa);
		$criteria->compare('TelefonePessoa',$this->TelefonePessoa,true);
		$criteria->compare('DataNascimentoPessoa',$this->DataNascimentoPessoa,true);
		$criteria->compare('EscolaridadePessoa',$this->EscolaridadePessoa);
		$criteria->compare('SenhaPessoa',$this->SenhaPessoa,true);
		$criteria->compare('SaldoPessoa',$this->SaldoPessoa,true);
		$criteria->compare('IndicadorProfessor',$this->IndicadorProfessor,true);
		$criteria->compare('IndicadorExcluido',$this->IndicadorExcluido,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Pessoa the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
