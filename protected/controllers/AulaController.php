﻿<?php

class AulaController extends Controller
{
	public function beforeAction($action) 
	{
		if( parent::beforeAction($action)) 
		{
			/* @var $cs CClientScript */
			$baseUrl = Yii::app()->baseUrl; 
			$cs = Yii::app()->clientScript;
			/* @var $theme CTheme */
			$cs->registerScriptFile($baseUrl . '/js/jquery.mask.js' );
			$cs->registerScriptFile($baseUrl . '/js/jquery.min.js' );
			return true;
		}
		
		return false;
	}
	
	public function actionIndex()
	{
		$this->render('index');
	}
	
	public function actionListaProfessores()
	{
		$pessoa = new Pessoa;
			
		if(isset($_POST['Filtro']))
		{
			$filtroDisc = $_POST['Filtro']['disciplina'];
			$filtroPrecoMin = $_POST['Filtro']['precoMin'];
			$filtroPrecoMax = $_POST['Filtro']['precoMax'];
			
			
			if($filtroPrecoMin > $filtroPrecoMax)
			{
				Yii::app()->user->setFlash('error', "O preço mínimo deve ser menor ou igual ao preço máximo!");
				
				$this->render('listaProfessores', array('lst_prof'=>$pessoa->buscaProfessores()));
			}
			else
			{
				$lst_prof = $pessoa->buscaProfessores(array('disciplina'=>$filtroDisc, 'precoMin'=>$filtroPrecoMin, 'precoMax'=>$filtroPrecoMax));
				
				$this->render('listaProfessores', array('lst_prof'=>$lst_prof));			
			}			
		}
		else
		{
			$this->render('listaProfessores', array('lst_prof'=>$pessoa->buscaProfessores()));			
		}
	}
	
	public function actionDetalharDisciplina()
	{
		if (isset($_GET['CodDisciplina']))
		{
			$disciplina = Disciplina::model()->findByPk($_GET['CodDisciplina']);
			$this->render('detalharDisciplina', array('disciplina'=>$disciplina));
		}
		else
		{
			Yii::app()->user->setFlash('error', "Disciplina não encontrada!");
			$this->redirect('listaProfessores');
		}
	}
	
	public function actionRenderFormEndereco()
	{
		$endereco = new Endereco;
		$estado = Estado::model()->findAll();
		$arrEstado = CHtml::listData($estado, 'CodEstado', 'UF');
		$this->renderPartial('/pessoa/formEndereco', array('endereco'=>$endereco, 'arrEstado'=>$arrEstado));
	}
	
	public function actionSalvarAula()
	{
		if (isset($_POST['Aula']))
		{
			$pessoa = Pessoa::model()->findByPk(Yii::app()->user->CodPessoa);
			
			if ($_POST['Disciplina']['PrecoDisciplina'] > $pessoa->SaldoPessoa)
			{
				Yii::app()->user->setFlash('error', "Saldo insuficiente para esta disciplina! Insira créditos!");
				//$this->render('listaProfessores', array('pessoa'=>$pessoa));
				$this->redirect('listaProfessores');
			}
			else
			{
				$pessoa->SaldoPessoa -= $_POST['Disciplina']['PrecoDisciplina'];

				// TODO: ACRESCENTAR SALDO PARA O PROFESSOR
				// TODO: ACRESCENTAR SALDO PARA O PERFIL DA ESCOLA (CRIA PERFIL DE ESCOLA)
				if ($_POST['opcao-local'] == 2)
				{
					$endereco = new Endereco;
					$endereco->attributes = $_POST['Endereco'];
					$endereco->save();
					$codEndereco = $endereco->CodEndereco;
				}
				else
				{
					$disciplina = Disciplina::model()->findByPK($_POST['Disciplina']['CodDisciplina']);
					$codEndereco = $disciplina->Professor->CodEnderecoPessoa;
				}
				
				//FORMATAR DATA
				$partesData = explode('/', $_POST['Aula']['DataAula']);
				$date = $partesData[2]."-".$partesData[1]."-".$partesData[0];
				
				$aula = new Aula;
				$aula->CodEndereco = $codEndereco;
				$aula->DataAula = $date;
				$aula->PrecoAula = $_POST['Disciplina']['PrecoDisciplina'];
				$aula->CodDisciplina = $_POST['Disciplina']['CodDisciplina'];
				$aula->CodPessoaAluno = Yii::app()->user->CodPessoa;
				$aula->IndicadorExcluido = 'N';
				$aula->IndicadorAulaRealizadaAluno = 'N';
				$aula->IndicadorAulaRealizadaProfessor = 'N';
				$aula->IndicadorAulaAceiteProfessor = 'P';
				$aula->save();
				
				$pessoa->save();
				Yii::app()->user->setState('SaldoPessoa', $pessoa->SaldoPessoa);
				Yii::app()->user->setFlash('success', "Aula marcada com sucesso!");
				$this->redirect('listaProfessores');
			}
		}
	}
	
	public function actionMinhasAulas()
	{
		$criteria = new CDbCriteria;		
		
		if (isset(Yii::app()->user->IndicadorAluno))
		{
			$criteria->addCondition('t.CodPessoaAluno = ' . Yii::app()->user->CodPessoa);
			$criteria->addCondition('t.IndicadorExcluido = "N"');
			$criteria->addCondition("t.IndicadorAulaRealizadaAluno = 'P'");
			$criteria->order = 't.DataAula';
		}
		else
		{
			$criteria = new CDbCriteria;
			$criteria->addCondition("t.IndicadorExcluido = 'N'");
			$criteria->addCondition("t.IndicadorAulaRealizadaProfessor = 'P'");
			$criteria->addCondition("Disciplina.CodPessoa = " . Yii::app()->user->CodPessoa);
		}
		
		$criteria->addCondition("t.IndicadorAulaAceiteProfessor <> 'P'");
		$aula = Aula::model()->with('Disciplina')->findAll($criteria);
		
		$dataProvider = new CArrayDataProvider($aula, array(
			'keyField'=>'CodAula',
			'id'=>'disc',
			'pagination'=>array(
				'pageSize'=>10,
			),
		));
		
		$this->render('minhasAulas', array('dp'=>$dataProvider));
	}
	
	public function actionAulaRealizada()
	{
		if(isset($_GET['CodAula']))
		{
			$aula = Aula::model()->findByPK($_GET['CodAula']);
			
			$realizada = $_GET['Realizada'];
			
			if(isset(Yii::app()->user->IndicadorAluno))
			{
				$aula->IndicadorAulaRealizadaAluno = $realizada;
			}
			else
			{
				$aula->IndicadorAulaRealizadaProfessor = $realizada;
			}
			
			$partesData = explode('/', $aula->DataAula);
			$aula->DataAula = $partesData[2]."-".$partesData[1]."-".$partesData[0];
				
			$aula->save();
									
			if($aula->IndicadorAulaRealizadaAluno == 'S' and $aula->IndicadorAulaRealizadaProfessor == 'S')
			{
				$pessoa = Pessoa::model()->findByPK($aula->Disciplina->CodPessoa);
				$pessoa->SaldoPessoa += ($aula->PrecoAula * 0.9);
				
				$pessoa->save();
				
				if(isset(Yii::app()->user->IndicadorProfessor))
				{
					Yii::app()->user->setState('SaldoPessoa', $pessoa->SaldoPessoa);
				}
			}
		}
		$this->redirect('minhasAulas');
	}
	
	public function actionListaAulasPendentes()
	{
		$pessoa = Pessoa::model()->findByPK(Yii::app()->user->CodPessoa);
			
		$this->render('listaAulasPendentes', array('lst_aulas'=>$pessoa->buscaAulasPendentes()));			
		
	}
	
	
	public function actionAceitarAula()
	{
		if(isset($_GET['CodAula']))
		{
			$aula = Aula::model()->findByPK($_GET['CodAula']);
			$aceitar = $_GET['Aceitar'];
			
			$aula->IndicadorAulaAceiteProfessor = $aceitar;
			
			$partesData = explode('/', $aula->DataAula);
			$aula->DataAula = $partesData[2]."-".$partesData[1]."-".$partesData[0];
			
			if($aula->IndicadorAulaAceiteProfessor == 'N')
			{
				$pessoa = Pessoa::model()->findByPK($aula->CodPessoaAluno);
				
				$pessoa->SaldoPessoa += $aula->PrecoAula;
				$pessoa->save();
				Yii::app()->user->setFlash('success', "Aula recusada!");
			}
			else
			{
				Yii::app()->user->setFlash('success', "Aula aceita!");
				$aula->IndicadorAulaRealizadaAluno = 'P';
				$aula->IndicadorAulaRealizadaProfessor = 'P';
			}
			
			$aula->save();
			
		}
		$this->redirect('listaAulasPendentes');
	}

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}