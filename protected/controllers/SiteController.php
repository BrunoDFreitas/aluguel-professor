﻿<?php

class SiteController extends Controller
{
	public function beforeAction($action) 
	{
		if( parent::beforeAction($action)) 
		{
			/* @var $cs CClientScript */
			$baseUrl = Yii::app()->baseUrl; 
			$cs = Yii::app()->clientScript;
			/* @var $theme CTheme */
			$cs->registerScriptFile($baseUrl . '/js/jquery.mask.js' );
			$cs->registerScriptFile($baseUrl . '/js/jquery.min.js' );
			return true;
		}
		
		return false;
	}
	
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		if (isset(Yii::app()->user->IndicadorProfessor))
			$this->render('indexProfessor');
		else
			if (isset(Yii::app()->user->IndicadorAluno))
				$this->render('indexAluno');
			else
				$this->render('index');
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	/**
	 * Displays the login page
	 */
	 
	public function actionNovoLogin()
	{
		if (isset($_POST['Pessoa']))
		{
			//CVarDumper::dump($_POST['Pessoa']);die;
			$username = $_POST['Pessoa']['CPF'];
			$password = $_POST['Pessoa']['Senha'];
					
			$identity = new UserIdentity($username,$password);
			
			if($identity->authenticate())
			{
				Yii::app()->user->login($identity);
				Yii::app()->user->setFlash('success', "Você está logado!");
				$this->redirect(array('site/index'));	
			}
			else
			{
				Yii::app()->user->setFlash('error', "Não foi possível logar-se! Verifique as informações preenchidas!");
				$this->redirect(array('site/novoLogin'));
			}
		}
		else
			$this->render('novoLogin');
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout($action = '')
	{
				Yii::app()->user->logout();
		if (!empty($action))
			Yii::app()->user->setFlash('success', "Cadastro excluído com sucesso!");
		else
			Yii::app()->user->setFlash('success', "Logout realizado com sucesso!");

		//$this->redirect(Yii::app()->homeUrl);
		$this->render('index');
	}
	
	/**
	 * Displays the login page
	 */
	 
	public function actionLoginFuncionario()
	{
		if (isset($_POST['Funcionario']))
		{
			//CVarDumper::dump($_POST['Funcionario']);die;
			$username = $_POST['Funcionario']['Usuario'];
			$password = $_POST['Funcionario']['Senha'];
					
			$identity = new FuncionarioIdentity($username,$password);
			
			if($identity->authenticate())
			{
				Yii::app()->user->login($identity);
				Yii::app()->user->setFlash('success', "Você está logado!");
				$this->redirect(array('funcionario/index'));
				
			}
			else
			{
				Yii::app()->user->setFlash('error', "Não foi possível logar-se! Verifique as informações preenchidas!");
				$this->redirect(array('site/loginFuncionario'));
			}
		}
		else
			$this->render('loginFuncionario');
	}
}