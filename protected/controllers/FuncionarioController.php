<?php

class FuncionarioController extends Controller
{
	public function beforeAction($action) 
	{
		if( parent::beforeAction($action)) 
		{
			/* @var $cs CClientScript */
			$baseUrl = Yii::app()->baseUrl; 
			$cs = Yii::app()->clientScript;
			/* @var $theme CTheme */
			$cs->registerScriptFile($baseUrl . '/js/jquery.mask.js' );
			$cs->registerScriptFile($baseUrl . '/js/jquery.min.js' );
			return true;
		}
		
		return false;
	}
	
	public function actionIndex()
	{
		$this->render('index');
	}
	
	public function actionCadastroFuncionario()
	{
		if(!isset($_POST['Funcionario']))
		{
			$funcionario = new Funcionario;
			
			$this->render('cadastroFuncionario', array('funcionario'=>$funcionario));
		}
		else
		{
			$senhaIguais = true;
			
			$funcionario = new Funcionario;
			$funcionario->attributes = $_POST['Funcionario'];
			
			if ($_POST['Funcionario']['NovaSenha'] == $_POST['Funcionario']['SenhaRepetida'])
				$funcionario->SenhaFuncionario = md5($_POST['Funcionario']['NovaSenha']);
			else
				$senhaIguais = false;
			
			if($senhaIguais)
			{
				if($funcionario->save())
				{
					Yii::app()->user->setFlash('success', "Funcionário cadastrado com sucesso!");
					$this->redirect(array('funcionario/index'));
				}
			}
			else
			{
				Yii::app()->user->setFlash('error', "As senhas informadas do funcionário são diferentes!");
			}
			$this->redirect('cadastroFuncionario');
		}
	}

	
	public function actionEstornarValores()
	{
		if(!isset(Yii::app()->user->IndicadorGerente))
		{
			Yii::app()->user->setFlash('error', "Usuário sem permissão para estornar valores!");
			$this->redirect('buscarClientes');
		}
		else
		{
			if (isset($_GET['CodPessoa']))
			{
				$pessoa = Pessoa::model()->findByPk($_GET['CodPessoa']);
				
				if($pessoa->IndicadorProfessor == 'S')
				{
					Yii::app()->user->setFlash('error', "Não é possível realizar estorno para professores!");
					$this->redirect('buscarClientes');
				}
				else
				{
					$cliente = $pessoa->NomePessoa;
					$saldo = $pessoa->SaldoPessoa;
					$valor = 0.00;
					$motivo = '';
					$this->render('formEstorno', array('pessoa'=>$pessoa));
				}
			}
			else
			{
				Yii::app()->user->setFlash('error', "Cliente não encontrado!");
				$this->redirect('buscarClientes');
			}
		}

		//$this->render('formEstorno');
	}
	
	public function actionSalvarEstorno()
	{	
		if(isset($_POST['Pessoa']))
		{
			$pessoa = Pessoa::model()->findByPk($_POST['Pessoa']['CodPessoa']);			
			
			if($_POST['Pessoa']['SaldoPessoa'] <= 0)
			{
				Yii::app()->user->setFlash('error', "O valor deve ser maior do que zero!");
				
			}
			else if($_POST['Pessoa']['SaldoPessoa'] > $pessoa->SaldoPessoa)
			{
				Yii::app()->user->setFlash('error', "Valor insuficiente a ser estornado!");
				
			}
			else
			{
				$pessoa->SaldoPessoa = $pessoa->SaldoPessoa - $_POST['Pessoa']['SaldoPessoa'];
				
				$pessoa->save();
				
				Yii::app()->user->setFlash('success', "Estorno de R$ ".$_POST['Pessoa']['SaldoPessoa']." realizado com sucesso!");			
			}
			
			$this->render('formEstorno', array('pessoa'=>$pessoa));
		}
		else
		{
			Yii::app()->user->setFlash('error', "Cliente não encontrado!");
			$this->redirect('index');
		}
	}
	
	
	public function actionBuscarClientes()
	{
		$pessoa = new Pessoa;
		
		if(isset($_POST['Filtro']))
		{
			$filtroNome = $_POST['Filtro']['nome'];
			
			$lst_clientes = $pessoa->buscaClientes(array('nome'=>$filtroNome));
				
			$this->render('BuscarClientes', array('lst_clientes'=>$lst_clientes));			
						
		}
		else
		{
			$this->render('BuscarClientes', array('lst_clientes'=>$pessoa->buscaClientes()));		
		}
	}
	
	
	public function actionDetalharCliente()
	{
		if (isset($_GET['CodPessoa']))
		{
			$cliente = Pessoa::model()->findByPk($_GET['CodPessoa']);
			$this->render('detalharCliente', array('cliente'=>$cliente));
		}
		else if(isset($_POST))
		{
			$this->redirect('buscarClientes');
		}
		else
		{
			Yii::app()->user->setFlash('error', "Cliente não encontrado!");
			$this->redirect('buscarClientes');
		}
	}
	
	public function actionListarProcessos()
	{
		$criteria = new CDbCriteria;
		$criteria->addCondition("t.IndicadorExcluido = 'N'");
		$criteria->addCondition("t.IndicadorAulaRealizadaAluno != t.IndicadorAulaRealizadaProfessor");
		
		$aulas = Aula::model()->with('Disciplina')->findAll($criteria);
		
		$dataProvider = new CArrayDataProvider($aulas, array(
			'keyField'=>'CodAula',
			'id'=>'aula',
			'pagination'=>array(
				'pageSize'=>10,
			),
		));
		//CVarDumper::dump($dataProvider,10,true);die;
		
		$this->render('listarProcessos', array('dp'=>$dataProvider));
	}
	
	public function actionEditarAula()
	{
		if (isset($_GET['CodAula']))
		{
			$aula = Aula::model()->with('Disciplina')->findByPk($_GET['CodAula']);
			$this->render('editarAula', array('aula'=>$aula));
		}
		else
		{
			if (isset($_POST['Aula']))
			{
				
				$aula = Aula::model()->findByPk($_POST['Aula']['CodAula']);
				if ($_POST['Aula']['Indicador'] == '1')
				{
					$professor = Pessoa::model()->findByPk($aula->Disciplina->CodPessoa);
					// TODO: PARTE DO VALOR VAI PARA A EMPRESA
					$professor->SaldoPessoa += ($aula->PrecoAula * 0.9);
					$professor->save();
					
					$aula->IndicadorAulaRealizadaAluno = 'S';
					$aula->IndicadorAulaRealizadaProfessor = 'S';
				}
				else
				{					
					$aluno = Pessoa::model()->findByPk($aula->CodPessoaAluno);
					$aluno->SaldoPessoa += $aula->PrecoAula;
					$aluno->save();
					
					$aula->IndicadorAulaRealizadaAluno = 'N';
					$aula->IndicadorAulaRealizadaProfessor = 'N';
				}
				
				$partesData = explode('/', $aula->DataAula);
				$aula->DataAula = $partesData[2]."-".$partesData[1]."-".$partesData[0];
				
				if (!$aula->save())
					Yii::app()->user->setFlash('error', "Não foi possível atualizar a aula!");
				else
					Yii::app()->user->setFlash('success', "Aula salva como realizada!");
				
				$this->redirect('listarProcessos');
			}
		}
	}
	
	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}