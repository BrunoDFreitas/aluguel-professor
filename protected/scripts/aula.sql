USE engsw;

DROP TABLE IF EXISTS `aula`;

CREATE TABLE `aula` (
  `CodAula` int(2) NOT NULL UNIQUE AUTO_INCREMENT,
  `CodDisciplina` int(2) NOT NULL,
  `CodPessoaAluno` int(2) NOT NULL,
  `DataAula` date NOT NULL,
  `PrecoAula` decimal(10,2) NOT NULL,
  `CodEndereco` int(2) NOT NULL,
  `IndicadorExcluido` varchar(1) NOT NULL,
  `IndicadorAulaRealizadaAluno` varchar(1) NOT NULL,
  `IndicadorAulaRealizadaProfessor` varchar(1) NOT NULL,
  `IndicadorAulaAceiteProfessor` varchar(1) NOT NULL,
	PRIMARY KEY (`CodAula`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*
INSERT INTO `aula` (`CodDisciplina`, `CodPessoaAluno`, `DataAula`, `PrecoAula`, `IndicadorAulaRealizadaAluno`, `IndicadorAulaRealizadaProfessor`, `IndicadorExcluido`, `CodEndereco`, `IndicadorAulaAceiteProfessor`) VALUES
(1, 2, '2016-05-31', 10.10, 'N', 'N', 'N', 3, 'P'),
(2, 2, '2016-05-31', 10.10, 'N', 'N', 'N', 4,'P'),
(3, 2, '2016-05-18', 100.50, 'N', 'N', 'N', 3,'P');

*/