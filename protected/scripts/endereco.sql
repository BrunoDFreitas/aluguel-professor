USE engsw;

DROP TABLE IF EXISTS `endereco`;

CREATE TABLE `endereco` (
  `CodEndereco` int(2) NOT NULL UNIQUE AUTO_INCREMENT,
  `Logradouro` varchar(50) NOT NULL,
  `Numero` int(5) DEFAULT NULL,
  `Complemento` varchar(20) DEFAULT NULL,
  `Bairro` varchar(30) NOT NULL,
  `CEP` varchar(9) NOT NULL,
  `Cidade` varchar(30) NOT NULL,
  `CodEstado` int(2) NOT NULL,
	PRIMARY KEY (`CodEndereco`)
);


INSERT INTO `endereco` (`Logradouro`, `Numero`, `Complemento`, `Bairro`, `CEP`, `Cidade`, `CodEstado`) VALUES
/*
('Rua tomas flores', 171, '801', 'bom fim', '90035201', 'poa', 1),
('A', 1, 123, '', '4', '5', 2),
('Rua Germano', 158, '', 'Azenha', '91919291', 'Porto Alegre', 3),
*/
('Endereço Bruno Professor', 123, 'Complemento', 'Bairro', '123456789', 'Teste', 1),
('Endereço Bruno Aluno', 123, 'Complemento', 'Bairro', '123456789', 'Teste', 1);
						

