USE engsw;

DROP TABLE IF EXISTS `escolaridade`;

CREATE TABLE `escolaridade` (
  `CodEscolaridade` int(2) NOT NULL UNIQUE AUTO_INCREMENT,
  `NomeEscolaridade` varchar(50) NOT NULL,
	PRIMARY KEY (`CodEscolaridade`)
);

INSERT INTO `escolaridade` (`NomeEscolaridade`) VALUES
('Analfabeto'),
('Ensino Fundamental Incompleto'),
('Ensino Fundamental Completo'),
('Ensino Médio Incompleto'),
('Ensino Médio Completo'),
('Ensino Superior Incompleto'),
('Graduado'),
('Mestrado'),
('Doutorado'),
('Especialização'),
('Pós-Doutorado'),
('Ensino Técnico');
