USE engsw;

DROP TABLE IF EXISTS `funcionario`;


CREATE TABLE `funcionario` (
  `CodFuncionario` int(2) NOT NULL UNIQUE AUTO_INCREMENT,
  `UsuarioFuncionario` varchar(20) NOT NULL UNIQUE,
  `NomeFuncionario` varchar(50) NOT NULL,
  `SenhaFuncionario` varchar(32) NOT NULL,
  `IndicadorGerente` varchar(1) NOT NULL,
  PRIMARY KEY (`CodFuncionario`)
);


-- Sanha do usuário master: master
-- Senha do usuário brunofunc e brunoger: teste
INSERT INTO `funcionario` (`UsuarioFuncionario`,`NomeFuncionario`,`SenhaFuncionario`,`IndicadorGerente`) VALUES
/*
('brunofunc', 'Bruno Funcionário', '698dc19d489c4e4db73e28a713eab07b', 'N'),
('brunoger', 'Bruno Gerente', '698dc19d489c4e4db73e28a713eab07b', 'S'),
*/
('master', 'Master', 'eb0a191797624dd3a48fa681d3061212','S');
