USE engsw;

DROP TABLE IF EXISTS `disciplina`;

CREATE TABLE `disciplina` (
  `CodDisciplina` int(2) NOT NULL UNIQUE AUTO_INCREMENT,
  `NomeDisciplina` varchar(20) NOT NULL,
  `AssuntoDisciplina` varchar(100) DEFAULT NULL,
  `DescricaoDisciplina` varchar(100) DEFAULT NULL,
  `PrecoDisciplina` decimal(10,2) NOT NULL,
  `CodPessoa` int(2) NOT NULL,
  `IndicadorExcluido` varchar(1) NOT NULL,
	PRIMARY KEY (`CodDisciplina`)
);

INSERT INTO `disciplina` (`NomeDisciplina`, `AssuntoDisciplina`, `DescricaoDisciplina`, `PrecoDisciplina`, `CodPessoa`, `IndicadorExcluido`) VALUES
/*
('Geografia', 'Assunto Geografia', 'Descrição Geografia', 100.50, 1, 'N'),
('Programação', 'Assunto Programação', 'Descrição Programação', 100.00, 3, 'N'),
('Programação', 'Assunto Programação', 'Descrição Programação', 100.00, 1, 'N'),
*/
('Física Básica', 'Assuntos abordados em Programação Básica', 'Descrição sobre as aulas de Programação Básica', 125.30, 1, 'N'),
('Física Avançada', 'Assuntos abordados em Programação Avançada', 'Descrição sobre as aulas de Programação Avançada', 230.00, 1, 'N');

