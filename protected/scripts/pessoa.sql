USE engsw;

DROP TABLE IF EXISTS `pessoa`;


CREATE TABLE `pessoa` (
  `CodPessoa` int(2) NOT NULL UNIQUE AUTO_INCREMENT,
  `NomePessoa` varchar(50) NOT NULL,
  `CPFPessoa` varchar(11) NOT NULL UNIQUE,
  `EmailPessoa` varchar(50) NOT NULL,
  `GeneroPessoa` varchar(1) NOT NULL,
  `CodEnderecoPessoa` int(2) NOT NULL,
  `TelefonePessoa` varchar(15) NOT NULL,
  `DataNascimentoPessoa` date NOT NULL,
  `EscolaridadePessoa` int(11) DEFAULT NULL,
  `SenhaPessoa` varchar(32) NOT NULL,
  `SaldoPessoa` decimal(10,2) NOT NULL,
  `IndicadorProfessor` varchar(1) NOT NULL,
  `IndicadorExcluido` varchar(1) NOT NULL,
	PRIMARY KEY (`CodPessoa`)
);

-- Senha das pessoas Bruno Professor e Bruno Aluno: teste

INSERT INTO `pessoa` (`NomePessoa`, `CPFPessoa`, `EmailPessoa`, `GeneroPessoa`, `CodEnderecoPessoa`, `TelefonePessoa`, `DataNascimentoPessoa`, `EscolaridadePessoa`, `SenhaPessoa`, `SaldoPessoa`, `IndicadorProfessor`, `IndicadorExcluido`) VALUES
/*
('Matheus Michel', '12345678971', 'mat@mat.com', 'M', 1, '(12) 3455-64564', '1990-01-01', 6, '698dc19d489c4e4db73e28a713eab07b', 50.00, 'S', 'N'),
('Rafael Allegretti', '12345678910', 'rafa@all.com', 'M', 2, '(12) 3455-64564', '1990-01-01', 5, '698dc19d489c4e4db73e28a713eab07b', 53.00, 'N', 'N'),
('Alberto Pena', '99999999999', 'alberto@pena.com', 'F', 3, '(12) 3455-64564', '1990-01-01', 3, '698dc19d489c4e4db73e28a713eab07b', 0.00, 'S', 'N'),
*/
('Bruno Professor', '11111111111', 'bruno@professor.com', 'M', 1, '(12) 3455-64564', '1990-01-01', 8, '698dc19d489c4e4db73e28a713eab07b', 125.00, 'S', 'N'),
('Bruno Aluno', '22222222222', 'bruno@aluno.com', 'M', 2, '(12) 3455-64564', '1990-01-01', 6, '698dc19d489c4e4db73e28a713eab07b', 300.00, 'N', 'N');
