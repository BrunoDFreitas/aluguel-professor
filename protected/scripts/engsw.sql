-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 22-Maio-2016 às 00:02
-- Versão do servidor: 10.1.13-MariaDB
-- PHP Version: 5.5.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `engsw`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `aula`
--

CREATE TABLE `aula` (
  `CodAula` int(2) NOT NULL,
  `CodDisciplina` int(2) NOT NULL,
  `CodPessoaAluno` int(2) NOT NULL,
  `DataAula` date NOT NULL,
  `IndicadorAulaRealizadaAluno` varchar(1) NOT NULL,
  `IndicadorAulaRealizadaProfessor` varchar(1) NOT NULL,
  `IndicadorExcluido` varchar(1) NOT NULL,
  `CodEndereco` int(2) NOT NULL,
  `IndicadorPendenteAluno` varchar(1) NOT NULL,
  `IndicadorPendenteProfessor` varchar(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `disciplina`
--

CREATE TABLE `disciplina` (
  `CodDisciplina` int(2) NOT NULL,
  `NomeDisciplina` varchar(20) NOT NULL,
  `AssuntoDisciplina` varchar(100) DEFAULT NULL,
  `DescricaoDisciplina` varchar(100) DEFAULT NULL,
  `PrecoDisciplina` decimal(5,2) NOT NULL,
  `CodPessoa` int(2) NOT NULL,
  `IndicadorExcluido` varchar(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `disciplina`
--

INSERT INTO `disciplina` (`CodDisciplina`, `NomeDisciplina`, `AssuntoDisciplina`, `DescricaoDisciplina`, `PrecoDisciplina`, `CodPessoa`, `IndicadorExcluido`) VALUES
(2, 'Geografia', 'Ola', 'Ola', '100.50', 1, 'N'),
(6, 'Programação', 'Teste', 'Teste', '100.00', 6, 'N'),
(7, 'Programação', 'Teste', 'Teste', '100.00', 1, 'N');

-- --------------------------------------------------------

--
-- Estrutura da tabela `endereco`
--

CREATE TABLE `endereco` (
  `CodEndereco` int(2) NOT NULL,
  `Logradouro` varchar(50) NOT NULL,
  `Numero` int(5) DEFAULT NULL,
  `Complemento` int(5) DEFAULT NULL,
  `Bairro` varchar(30) NOT NULL,
  `CEP` varchar(9) NOT NULL,
  `Cidade` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `endereco`
--

INSERT INTO `endereco` (`CodEndereco`, `Logradouro`, `Numero`, `Complemento`, `Bairro`, `CEP`, `Cidade`) VALUES
(2, 'Rua tomas flores', 171, 801, 'bom fim', '90035201', 'poa'),
(3, 'A', 1, 2, '3', '4', '5'),
(4, 'Rua Germano', 158, NULL, 'Azenha', '91919291', 'Porto Alegre');

-- --------------------------------------------------------

--
-- Estrutura da tabela `escolaridade`
--

CREATE TABLE `escolaridade` (
  `CodEscolaridade` int(2) NOT NULL,
  `NomeEscolaridade` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `escolaridade`
--

INSERT INTO `escolaridade` (`CodEscolaridade`, `NomeEscolaridade`) VALUES
(1, 'Analfabeto'),
(2, 'Ensino Fundamental Incompleto'),
(3, 'Ensino Fundamental Completo'),
(4, 'Ensino M?dio Incompleto'),
(5, 'Ensino M?dio Completo'),
(6, 'Ensino Superior Incompleto'),
(7, 'Gradua??o'),
(8, 'Mestrado'),
(9, 'Doutorado'),
(10, 'Especializa??o'),
(11, 'P?s-Doutorado'),
(12, 'Ensino T?cnico');

-- --------------------------------------------------------

--
-- Estrutura da tabela `pessoa`
--

CREATE TABLE `pessoa` (
  `CodPessoa` int(2) NOT NULL,
  `NomePessoa` varchar(20) NOT NULL,
  `CPFPessoa` varchar(11) NOT NULL,
  `EmailPessoa` varchar(20) NOT NULL,
  `GeneroPessoa` varchar(1) NOT NULL,
  `CodEnderecoPessoa` int(2) NOT NULL,
  `TelefonePessoa` varchar(13) NOT NULL,
  `DataNascimentoPessoa` date NOT NULL,
  `EscolaridadePessoa` int(11) DEFAULT NULL,
  `SenhaPessoa` varchar(32) NOT NULL,
  `SaldoPessoa` decimal(10,2) NOT NULL,
  `IndicadorProfessor` varchar(1) NOT NULL,
  `IndicadorFuncionario` varchar(1) NOT NULL,
  `IndicadorExcluido` varchar(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `pessoa`
--

INSERT INTO `pessoa` (`CodPessoa`, `NomePessoa`, `CPFPessoa`, `EmailPessoa`, `GeneroPessoa`, `CodEnderecoPessoa`, `TelefonePessoa`, `DataNascimentoPessoa`, `EscolaridadePessoa`, `SenhaPessoa`, `SaldoPessoa`, `IndicadorProfessor`, `IndicadorFuncionario`, `IndicadorExcluido`) VALUES
(1, 'Matheus Michel', '12345678971', 'mat@mat.com', 'M', 3, '33113973', '0000-00-00', 6, '202cb962ac59075b964b07152d234b70', '50.00', 'S', 'N', 'N'),
(2, 'Rafael Allegretti', '12345678910', 'rafa@all.com', '', 2, '', '0000-00-00', NULL, '21232f297a57a5a743894a0e4a801fc3', '53.00', 'N', 'N', 'N'),
(6, 'Alberto Pena', '22222222222', 'bruno@freitas.com', '', 4, '', '0000-00-00', NULL, '21232f297a57a5a743894a0e4a801fc3', '0.00', 'S', 'N', 'N');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `aula`
--
ALTER TABLE `aula`
  ADD PRIMARY KEY (`CodAula`);

--
-- Indexes for table `disciplina`
--
ALTER TABLE `disciplina`
  ADD PRIMARY KEY (`CodDisciplina`);

--
-- Indexes for table `endereco`
--
ALTER TABLE `endereco`
  ADD PRIMARY KEY (`CodEndereco`);

--
-- Indexes for table `escolaridade`
--
ALTER TABLE `escolaridade`
  ADD PRIMARY KEY (`CodEscolaridade`);

--
-- Indexes for table `pessoa`
--
ALTER TABLE `pessoa`
  ADD PRIMARY KEY (`CodPessoa`),
  ADD UNIQUE KEY `CPFPessoa` (`CPFPessoa`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `aula`
--
ALTER TABLE `aula`
  MODIFY `CodAula` int(2) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `disciplina`
--
ALTER TABLE `disciplina`
  MODIFY `CodDisciplina` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `endereco`
--
ALTER TABLE `endereco`
  MODIFY `CodEndereco` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `escolaridade`
--
ALTER TABLE `escolaridade`
  MODIFY `CodEscolaridade` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `pessoa`
--
ALTER TABLE `pessoa`
  MODIFY `CodPessoa` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
