<fieldset>
<legend>Estornar Valores</legend>
<?php
	echo CHtml::beginForm(Yii::app()->createAbsoluteUrl("funcionario/SalvarEstorno"), 'POST', array());
	echo CHtml::activeHiddenField($pessoa, 'CodPessoa', array());
	echo CHtml::activeHiddenField($pessoa, 'SaldoPessoa', array());
	echo CHtml::label('Cliente: ', 'label_saldo', array());
	echo CHtml::label($pessoa->NomePessoa, '');
	echo "<br />";
	echo CHtml::label('Saldo atual: ', 'label_saldo', array());
	echo CHtml::label($pessoa->SaldoPessoa, '');
	echo "<br />";
	echo CHtml::label('Valor a ser estornado: ', 'label_valor', array());
	echo CHtml::activeTextField($pessoa, 'SaldoPessoa', array());
	echo "<br />";
	/*
	echo CHtml::label('Motivo do estorno: ', 'label_deposito', array());
	echo CHtml::textField($motivo, "", array());
	echo "<br />";
	*/
	echo CHtml::submitButton('Enviar', array('class' => 'btn', 'style'=>'margin-left: 5px;'));
?>
</fieldset>