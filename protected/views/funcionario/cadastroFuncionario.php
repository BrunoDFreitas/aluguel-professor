<style>
div.form-perfil {
    text-align: center;
}
</style>

<?php
	$legend = 'Cadastro de Funcionário';
?>
<fieldset>
	<legend><?=$legend;?></legend>
	<div class="form-perfil">
		<?php
			echo CHtml::beginForm(Yii::app()->createAbsoluteUrl("funcionario/cadastroFuncionario"), 'POST', array());
			
			//echo CHtml::label('Tipo Funcionário: ', 'label_categoria');
			//echo CHtml::radioButtonList('Funcionario[Indicador]', '', array(1=>'Aluno(a)', 2=>'Professor(a)'), array('required'=>true, 'separator' => " "));
			
		?>
		<div class="column medium-7" style="text-align: right; margin-right:-10px;">
			<?php
				echo "<br />";
				
				echo CHtml::activeHiddenField($funcionario, 'CodFuncionario', array());
				echo "<br />";
				
				echo CHtml::label('Gerente: ', 'label_gerente');
				echo CHtml::activeCheckBox($funcionario, 'IndicadorGerente', array('value'=>'S', 'uncheckValue'=>'N', 'style'=>'margin-right: 180px; margin-bottom: 8px;'));
				echo "<br />";
				
				echo CHtml::label('Nome: ', 'label_nome');
				echo CHtml::activeTextField($funcionario, 'NomeFuncionario', array('required'=>true, 'maxlenght'=>50, 'style'=>'margin-bottom: 8px; margin-right: 24px;'));
				echo "<br />";
									
				echo CHtml::label('Usuario: ', 'label_usuario');
				echo CHtml::activeTextField($funcionario, 'UsuarioFuncionario', array('required'=>true, 'maxlenght'=>20, 'style'=>'margin-bottom: 8px; margin-right: 24px;'));
				echo "<br />";
			
				echo CHtml::label('Senha: ', 'label_senha');
				echo CHtml::passwordField('Funcionario[NovaSenha]', '', array('required'=>true, 'maxlenght'=>14, 'style'=>'margin-bottom: 8px; margin-right: 24px;'));
				echo "<br />";
				
				echo CHtml::label('Confirmar senha: ', 'label_senha2');
				echo CHtml::passwordField('Funcionario[SenhaRepetida]', '', array('required'=>true, 'maxlenght'=>14, 'style'=>'margin-bottom: 8px; margin-right: 24px;'));
				echo "<br /><br />";
			?>	
		</div>
		<div class="column medium-5" style="text-align: right; height: 195px;"></div>
		<?php echo CHtml::submitButton('Incluir Funcionário', array('class' => 'btn',)); ?>
	</div>
</fieldset>
<!--
<script>
	// MÁSCARAS
	$('#Pessoa_DataNascimentoPessoa').mask('00/00/0000');
	$('#Pessoa_CPFPessoa').mask('00000000000');
	$('#Pessoa_TelefonePessoa').mask('(00) 0000-00009');
	$('#Pessoa_TelefonePessoa').blur(function(event) {
	   if($(this).val().length == 15){ // Celular com 9 dígitos + 2 dígitos DDD e 4 da máscara
		  $('#Pessoa_TelefonePessoa').mask('(00) 00000-0009');
	   } else {
		  $('#Pessoa_TelefonePessoa').mask('(00) 0000-00009');
	   }
	});
	$('#Pessoa_DataNascimentoPessoa').mask('00/00/0000');
</script>
-->