<fieldset class="fieldset-1"><legend style="font-size: 150%;">Detalhes da Aula</legend>
	<?php
		$this->widget('zii.widgets.CDetailView', array(
			'nullDisplay'=>'',
			'data'=>$aula,
			'attributes'=>array(
				array(
					'label' => 'Professor',
					'value' => $aula->Disciplina->Professor->NomePessoa,
				),
				array(
					'label'=>'Data da aula',
					'value'=> $aula->DataAula,
				),
				array(
					'label'=>'Valor da aula',
					'value'=>$aula->PrecoAula,
				),
				array(
					'label'=>'Aula confirmada pelo aluno',
					'value'=> ($aula->IndicadorAulaRealizadaAluno == 'S' ? 'Sim' : 'Não'),
				),
				array(
					'label'=>'Aula confirmada pelo aluno',
					'value'=> ($aula->IndicadorAulaRealizadaProfessor == 'S' ? 'Sim' : 'Não'),
				),
			),
		));
		
		echo "<br />";
		echo CHtml::beginForm(Yii::app()->createAbsoluteUrl("funcionario/editarAula"), 'POST', array());
		echo CHtml::activeHiddenField($aula, 'CodAula', array());
		echo CHtml::activeHiddenField($aula, 'PrecoAula', array());

		echo CHtml::radioButtonList('Aula[Indicador]', 1, array('1'=>'Aula ocorreu', '2'=>'Aula não ocorreu'), array('required'=>true, 'separator' => " "));
		echo "<br /><br />";
		echo CHtml::submitButton('Enviar', array());
		echo CHtml::endForm();
	?>
</fieldset>
	