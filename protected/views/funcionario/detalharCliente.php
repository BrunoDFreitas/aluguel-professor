﻿<fieldset class="fieldset-1"><legend style="font-size: 150%;">Detalhes do Cliente</legend>
<?php
	$partesData = explode('-', $cliente->DataNascimentoPessoa);
	$data = $partesData[2]."/".$partesData[1]."/".$partesData[0];
				
	
	$this->widget('zii.widgets.CDetailView', array(
		'nullDisplay'=>'',
		'data'=>$cliente,
		'attributes'=>array(
			array(
				'label' => 'Nome',
				'value' => $cliente->NomePessoa,
			),
			array(
				'label' => 'CPF',
				'value' => $cliente->CPFPessoa,
			),
			array(
				'label' => 'Professor',
				'value' => $cliente->IndicadorProfessor,
			),
			array(
				'label' => 'E-mail',
				'value' => $cliente->EmailPessoa,
			),
			array(
				'label' => 'Gênero',
				'value' => $cliente->GeneroPessoa,
			),
			array(
				'label' => 'Telefone',
				'value' => $cliente->TelefonePessoa,
			),
			array(
				'label' => 'Nascimento',
				'value' => $data,
			),
			array(
				'label' => 'Escolaridade',
				'value' => $cliente->Escolaridade->NomeEscolaridade,
			),
			array(
				'label' => 'Logradouro',
				'value' => $cliente->Endereco->Logradouro,
			),
			array(
				'label' => 'Número',
				'value' => $cliente->Endereco->Numero,
			),
			array(
				'label' => 'Complemento',
				'value' => $cliente->Endereco->Complemento,
			),
			array(
				'label' => 'CEP',
				'value' => $cliente->Endereco->CEP,
			),
			array(
				'label' => 'Bairro',
				'value' => $cliente->Endereco->Bairro,
			),
			array(
				'label' => 'Cidade',
				'value' => $cliente->Endereco->Cidade,
			),
		),
	));
	?>
	
	<br /><br />
	<?php
	/*
		echo CHtml::beginForm(Yii::app()->createAbsoluteUrl("aula/salvarAula"), 'POST', array());
		echo CHtml::activeHiddenField($cliente, 'CodDisciplina', array());
		echo CHtml::activeHiddenField($cliente, 'PrecoDisciplina', array());
		
		echo CHtml::label('Local da aula: ', 'label_local');
		echo CHtml::radioButtonList('opcao-local', 1, array(1=>'Local definido pelo professor', 2=>'Novo local'), array('required'=>true, 'separator' => " "));
		echo "<br />";

		echo CHtml::label('Data da aula: ', '', array());
		$this->widget('zii.widgets.jui.CJuiDatePicker', array(
			'name' => 'Aula[DataAula]',
			'value'=>'',
			'options' => array(
				'showAnim' => 'slideDown',
				'dateFormat'=>'dd/mm/yy',
			),
			'language' => 'pt',
		));
		echo "<br />";
		*/
	?>
	<div id='novo-local' style="display: none">
	</div>
		
	<?php
		echo CHtml::beginForm(Yii::app()->createAbsoluteUrl("funcionario/detalharCliente"), 'POST', array());
	
		echo CHtml::submitButton('Voltar', array());
	?>
</fieldset>
