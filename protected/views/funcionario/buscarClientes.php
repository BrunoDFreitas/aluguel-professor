﻿<fieldset style="margin-top: -48px;"><legend>Buscar Clientes</legend>
<div>	
	<?php
		echo CHtml::beginForm(Yii::app()->createAbsoluteUrl("funcionario/buscarClientes"), 'POST', array());
		
		echo CHtml::label('FILTROS', 'label_filtros');
		echo "<br />";
		echo "<br />";
		
		echo CHtml::label('Nome: ', 'label_disciplina');
		echo CHtml::textField('Filtro[nome]', '', array('maxlenght'=>50, 'style'=>'margin-bottom: 8px;'));
		echo "<br />";
		
		echo "<br />";
		echo CHtml::submitButton('Filtrar', array('class' => 'btn',));
		echo "<br />";
		
	?>
</div>
<div>
	<?php
		$this->widget('ext.GroupGridView', array(
			'id' => 'grid1',
			'dataProvider' => $lst_clientes,
			//'mergeColumns' => array('NomePessoa'),
			//'mergeType'=>'nested',
			'enableSorting' => false,
			'summaryText' => '',
			'columns' => array(
				array(
					'header'=>'Nome',
					'name'=>'NomePessoa',
				),
				array(
					'header'=> 'Tipo',
					'name'=>'IndicadorProfessor'
				),
				array
				(
					'header'=>'Operações',
					'class'=>'CButtonColumn',
					'template'=>'{view}{update}',
					'buttons'=>array(
						'view'=>array(
							'url'=>'Yii::app()->createUrl("funcionario/detalharCliente/", array("CodPessoa"=>$data->CodPessoa))',
						),
						'update'=>array(
							'url'=>'Yii::app()->createUrl("funcionario/estornarValores/", array("CodPessoa"=>$data->CodPessoa))',
						),
					),
				),
			),
		));
	?>
</div>
</fieldset>