<fieldset>
<legend>Investigação de aulas</legend>
<div style='margin-top: -20px;'>
<?php
	$this->widget('zii.widgets.grid.CGridView', array(
		'dataProvider'=>$dp,
		'columns'=>array(
			array(
				'header'=>'Nome do professor',
				'value'=>'$data->Disciplina->Professor->NomePessoa',
				'htmlOptions'=>array('style'=>'text-align:center;'),
			),
			array(
				'header'=>'Nome da Disciplina',
				'value'=>'$data->Disciplina->NomeDisciplina',
				'htmlOptions'=>array('style'=>'text-align:center;'),
			),
			array(
				'header'=>'Data da aula',
				'name'=>'DataAula',
				'htmlOptions'=>array('style'=>'text-align:center;'),
			),
			array(
				'header'=>'Valor da aula',
				'name'=>'PrecoAula',
				'htmlOptions'=>array('style'=>'text-align:center;'),
			),
			array(
				'header'=>'Aula confirmada pelo aluno',
				'type'=>'html',
				'value'=>'($data->IndicadorAulaRealizadaAluno == "S") ? CHtml::image(Yii::app()->request->baseUrl . "/img/certo.jpg", "",array("style"=>"width:15px;height:15px;")):CHtml::image(Yii::app()->request->baseUrl . "/img/errado.jpg", "",array("style"=>"width:15px;height:15px;"))',
				'htmlOptions'=>array('style'=>'text-align:center;'),
			),
			array(
				'header'=>'Aula confirmada pelo professor',
				'type'=>'html',
				'value'=>'($data->IndicadorAulaRealizadaProfessor == "S") ? CHtml::image(Yii::app()->request->baseUrl . "/img/certo.jpg", "",array("style"=>"width:15px;height:15px;")):CHtml::image(Yii::app()->request->baseUrl . "/img/errado.jpg", "",array("style"=>"width:15px;height:15px;"))',
				'htmlOptions'=>array('style'=>'text-align:center;'),
			),
			array
			(
				'header'=>'Operações',
				'class'=>'CButtonColumn',
				'template'=>'{update}',
				'buttons'=>array(
					'update'=>array(
						'url'=>'Yii::app()->createUrl("funcionario/editarAula", array("CodAula"=>$data->CodAula))',	
					),
				),
			),
		),
	));
?>
</div>
</fieldset>