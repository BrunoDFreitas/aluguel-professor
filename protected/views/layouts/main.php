﻿<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="language" content="en">

	<!-- blueprint CSS framework -->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection">
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print">
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection">
	<![endif]-->

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css">
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css">
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/views.css">
	<?php
		$baseUrl = Yii::app()->baseUrl; 
		$cs = Yii::app()->getClientScript();
		$cs->registerScriptFile($baseUrl.'/js/jquery.mask.min.js');
		$cs->registerScriptFile($baseUrl.'/js/jquery.min.js');
	?>
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body>

<div class="container" id="page">
	<div id="header">
		<div id="logo">
			<?php echo CHtml::encode(Yii::app()->name); 
				if (!Yii::app()->user->isGuest)
				{
					if (!isset(Yii::app()->user->IndicadorFuncionario))
					{
						if (Yii::app()->user->SaldoPessoa >= 0)
							$msg = "<span style='color: #006600;'><b>" . Yii::app()->user->SaldoPessoa . '</b></span>';
						else
							$msg = "<span style='color: red;'><b>" . Yii::app()->user->SaldoPessoa . '</b></span>';
						echo "<p style='font-size: 14px;'>Bem-vindo <b>" . Yii::app()->user->NomePessoa . "</b>, seu saldo atual é de R$ " . $msg;
					}
				}
			?>
		</div>
	</div><!-- header -->

	<div id="mainmenu">
		<?php 
		
			/*$this->widget('zii.widgets.CMenu',array(
				'items'=>array(
					array('label'=>'Home', 'url'=>array('/site/index')),
					array('label'=>'About', 'url'=>array('/site/page', 'view'=>'about')),
					array('label'=>'Contact', 'url'=>array('/site/contact')),
					array('label'=>'Login', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),
					array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest)
				),
			));	*/

			$this->widget('zii.widgets.CMenu',array(
				'items'=>array(
					array('label'=>'USUÁRIO', 'visible'=>(Yii::app()->user->isGuest or !isset(Yii::app()->user->IndicadorFuncionario)), 'items'=>array(
						array('label'=>'Login', 'url'=>array('/site/novoLogin'), 'visible'=>Yii::app()->user->isGuest),
						array('label'=>'Logout ('. (!isset(Yii::app()->user->NomePessoa) ? '' : Yii::app()->user->NomePessoa).')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest),
						array('label'=>Yii::app()->user->isGuest ? 'Cadastrar' : 'Meu Perfil', 'url'=>array('/pessoa/meuPerfil')),
						array('label'=>'Inserir Créditos', 'url'=>array('/pessoa/inserirCredito'), 'visible'=>isset(Yii::app()->user->IndicadorAluno)),
						array('label'=>'Sacar Créditos', 'url'=>array('/pessoa/sacarCredito'), 'visible'=>isset(Yii::app()->user->IndicadorProfessor)),
						),
					),
					array('label'=>'DISCIPLINAS', 'visible' => isset(Yii::app()->user->IndicadorProfessor), 'items'=>array(
						array('label'=>'Minhas Disciplinas', 'url'=>array("/disciplina/minhasDisciplinas")),
						array('label'=>'Cadastrar Disciplinas', 'url'=>array("/disciplina/viewOrNewDisciplina")),	
						),		
					),
					array('label'=>'AULAS', 'visible' => (isset(Yii::app()->user->IndicadorAluno) || isset(Yii::app()->user->IndicadorProfessor)), 'items'=>array(
						array('label'=>'Aulas Agendadas', 'url'=>array("/aula/minhasAulas"), 'visible' => true),
						array('label'=>'Aulas Pendetes', 'url'=>array("/aula/listaAulasPendentes"), 'visible' => true),
						array('label'=>'Listar Professores', 'url'=>array("/aula/listaProfessores"), 'visible' => isset(Yii::app()->user->IndicadorAluno)),
						),		
					),
					array('label'=>'ADMIN', 'visible'=>(Yii::app()->user->isGuest || isset(Yii::app()->user->IndicadorFuncionario)), 'items'=>array(
						array('label'=>'Login de Funcionário', 'url'=>array('/site/loginFuncionario'), 'visible'=>Yii::app()->user->isGuest),
						array('label'=>'Logout ('. (!isset(Yii::app()->user->NomeFuncionario) ? '' : Yii::app()->user->NomeFuncionario).')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest),
						array('label'=>'Buscar Clientes', 'url'=>array('/funcionario/buscarClientes'), 'visible'=>!Yii::app()->user->isGuest),
						array('label'=>'Investigar aulas', 'url'=>array('/funcionario/listarProcessos'), 'visible'=>!Yii::app()->user->isGuest),
						),
					),
					array('label'=>'GERENTE', 'visible'=>(isset(Yii::app()->user->IndicadorGerente)), 'items'=>array(
						array('label'=>'Cadastro de Funcionário', 'url'=>array('/funcionario/cadastroFuncionario')),
						),
					),
				),
			));
		?>
	</div><!-- mainmenu -->
	<div class='flash'>
	<?php
		$flashMessages = Yii::app()->user->getFlashes();
		if ($flashMessages) {
			echo '<ul class="flashes">';
			foreach($flashMessages as $key => $message) {
				echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
			}
			echo '</ul>';
		}
	?>
	</div>
	<?php if(isset($this->breadcrumbs)):?>
		<?php $this->widget('zii.widgets.CBreadcrumbs', array(
			'links'=>$this->breadcrumbs,
		)); ?><!-- breadcrumbs -->
	<?php endif?>

	<?php echo $content; ?>

	<div class="clear"></div>
</div><!-- page -->

</body>
</html>
