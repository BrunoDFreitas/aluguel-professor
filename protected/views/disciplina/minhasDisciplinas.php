<fieldset>
<legend>Minhas Disciplinas</legend>
<?php
	$this->widget('zii.widgets.grid.CGridView', array(
		'dataProvider'=>$dp,
		'columns'=>array(
			array(
				'header'=>'Nome da Disciplina',
				'value'=>'$data->NomeDisciplina',
			),
			array(
				'header'=>'Assunto',
				'name'=>'AssuntoDisciplina',
			),
			array(
				'header'=>'Descrição',
				'name'=>'DescricaoDisciplina',
			),
			array(
				'header'=>'Valor Hora-Aula (R$)',
				'name'=>'PrecoDisciplina',
			),
			array
			(
				'header'=>'Operações',
				'class'=>'CButtonColumn',
				'template'=>'{update}{delete}',
				'buttons'=>array(
					'update'=>array(
						'url'=>'Yii::app()->createUrl("disciplina/viewOrNewDisciplina", array("CodDisciplina"=>$data->CodDisciplina))',	
					),
					'delete'=>array(
						'url'=>'Yii::app()->createUrl("disciplina/deletarDisciplina", array("CodDisciplina"=>$data->CodDisciplina))',
					),
				),
			),
		),
	));
?>
</fieldset>