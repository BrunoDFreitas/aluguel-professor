﻿<fieldset style="margin-top: -47px;">
<?php
	if ($disciplina->isNewRecord)
		$legend = 'Cadastrar Disciplina';
	else
		$legend = 'Editar Disciplina';
	
	echo '<legend>' . $legend . '</legend>';
?>
<div class="row">
	<div class="column medium-5" style="text-align: right; margin-right: 10px;">
<?php
	echo CHtml::beginForm(Yii::app()->createAbsoluteUrl("disciplina/salvarDisciplina"), 'POST', array());
	echo CHtml::activeHiddenField($disciplina, 'CodDisciplina', array());
	echo CHtml::label('Nome da disciplina: ', 'label_nome_disc', array('required'=>true));
	echo CHtml::activeTextField($disciplina, "NomeDisciplina", array());
	echo "<br /><br />";
	
	echo CHtml::label('Assunto: ', 'label_assunto', array('style'=>'vertical-align: top;'));
	echo CHtml::activeTextArea($disciplina, "AssuntoDisciplina", array('rows'=>4, 'style'=>'width: 167px;', 'maxlength'=>100));
	echo "<br />";
?>
</div>
<div class="column medium-4" style="text-align: right;">
<?php	
	echo CHtml::label('Preço: ', 'label_preco_disc');
	echo CHtml::activeTextField($disciplina, "PrecoDisciplina", array('required'=>true));
	echo "<br /><br />";
		
	echo CHtml::label('Descrição: ', 'label_descricao', array('style'=>'vertical-align: top;'));
	echo CHtml::activeTextArea($disciplina, "DescricaoDisciplina", array('rows'=>4, 'style'=>'width: 167px;', 'maxlength'=>100));
	echo "<br />";
?></div>
<div class="column medium-3" style="text-align: right;">
</div>
</div>
<div style="text-align:center;">
<?php
	echo "<br><br>";
	echo CHtml::submitButton('Enviar', array('class' => 'btn', 'style'=>''));
?>
</div>
</fieldset>