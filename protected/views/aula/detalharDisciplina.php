﻿<fieldset class="fieldset-1"><legend style="font-size: 150%;">Detalhes da Disciplina</legend>
<?php
	$this->widget('zii.widgets.CDetailView', array(
		'nullDisplay'=>'',
		'data'=>$disciplina,
		'attributes'=>array(
			array(
				'label' => 'Professor',
				'value' => $disciplina->Professor->NomePessoa,
			),
			'NomeDisciplina',
			'AssuntoDisciplina',
			'DescricaoDisciplina',
			'PrecoDisciplina',
			array(
				'label' => 'Logradouro',
				'value' => $disciplina->Professor->Endereco->Logradouro,
			),
			array(
				'label' => 'Número',
				'value' => $disciplina->Professor->Endereco->Numero,
			),
			array(
				'label' => 'Complemento',
				'value' => $disciplina->Professor->Endereco->Complemento,
			),
			array(
				'label' => 'CEP',
				'value' => $disciplina->Professor->Endereco->CEP,
			),
			array(
				'label' => 'Bairro',
				'value' => $disciplina->Professor->Endereco->Bairro,
			),
			array(
				'label' => 'Cidade',
				'value' => $disciplina->Professor->Endereco->Cidade,
			),
		),
	));
	?>
	<br /><br />
	<legend>Marcar Aula</legend>
	<?php
		echo CHtml::beginForm(Yii::app()->createAbsoluteUrl("aula/salvarAula"), 'POST', array());
		echo CHtml::activeHiddenField($disciplina, 'CodDisciplina', array());
		echo CHtml::activeHiddenField($disciplina, 'PrecoDisciplina', array());
		
		echo CHtml::label('Local da aula: ', 'label_local');
		echo CHtml::radioButtonList('opcao-local', 1, array(1=>'Local definido pelo professor', 2=>'Novo local'), array('required'=>true, 'separator' => " "));
		echo "<br />";

		echo CHtml::label('Data da aula: ', '', array());
		$this->widget('zii.widgets.jui.CJuiDatePicker', array(
			'name' => 'Aula[DataAula]',
			'value'=>'',
			'options' => array(
				'showAnim' => 'slideDown',
				'dateFormat'=>'dd/mm/yy',
			),
			'language' => 'pt',
			'htmlOptions'=>array('required'=>true),
		));
		echo "<br />";
	?>
	<div id='novo-local' style="display: none">
	</div>
		
	<?php
		echo CHtml::submitButton('Enviar', array());
	?>
</fieldset>
<script>
	$("#opcao-local").change(function() {
		var option = $('input:radio[name=opcao-local]:checked').val();
		$("#novo-local").empty();
		if (option == '2')
		{
			$.ajax({
				type: 'POST',
				url: '<?php echo $this->createAbsoluteUrl('aula/renderFormEndereco');?>',
				success:function(html){
					$("#novo-local").html(html);
					$("#novo-local").show();
				},
				dataType:'html',
			});
		}
	});
</script>