﻿<fieldset style="margin-top: -48px;"><legend>Aulas Pendentes de Aceitação</legend>
<div>	
	<?php
	/*
		echo CHtml::beginForm(Yii::app()->createAbsoluteUrl("aula/listaAulasPendentes"), 'POST', array());
		
		echo CHtml::label('FILTROS', 'label_filtros');
		echo "<br />";
		echo "<br />";
		
		echo CHtml::label('Disciplina: ', 'label_disciplina');
		echo CHtml::textField('Filtro[disciplina]', '', array('maxlenght'=>20, 'style'=>'margin-bottom: 8px;'));
		echo "<br />";
		
		echo CHtml::label('Preço Mínimo: ', 'label_preco_min');
		echo CHtml::textField('Filtro[precoMin]', '0.00', array('style'=>'margin-bottom: 8px;'));
		echo "<br />";
		
		echo CHtml::label('Preço Máximo: ', 'label_preco_max');
		echo CHtml::textField('Filtro[precoMax]', '999.99', array('style'=>'margin-bottom: 8px;'));
		echo "<br />";
		
		echo CHtml::submitButton('Filtrar', array('class' => 'btn',));
		echo "<br />";
		
	*/
	?>
</div>
<div>
	<?php
		$this->widget('ext.GroupGridView', array(
			'id' => 'grid1',
			'dataProvider' => $lst_aulas,
			'mergeColumns' => array('NomePessoa'),
			//'mergeType'=>'nested',
			'enableSorting' => false,
			'summaryText' => '',
			'columns' => array(
				array(
					'header'=>isset(Yii::app()->user->IndicadorProfessor) ? 'Aluno' : 'Professor',
					'name'=>'NomePessoa',
					'htmlOptions'=>array('style'=>'text-align:center;'),
				),
				array(
					'header'=> 'Nome da Disciplina',
					'name'=>'NomeDisciplina',
					'htmlOptions'=>array('style'=>'text-align:center;'),
				),
				array(
					'header'=> 'Preço (R$)',
					'name'=>'PrecoAula',
					'htmlOptions'=>array('style'=>'text-align:center;'),
				),
				array(
					'header'=> 'Dia',
					'name'=>'DataAula',
					'htmlOptions'=>array('style'=>'text-align:center;'),
				),
				
				array
				(
					'header'=>'Aceitar aula',
					'class'=>'CButtonColumn',
					'template'=>'{Aceitar}',
					'visible'=> isset(Yii::app()->user->IndicadorProfessor) ? true : false,
					'buttons'=>array(
						'Aceitar'=>array(
							'url'=>'Yii::app()->createUrl("aula/aceitarAula/", array("CodAula"=>$data->CodAula, "Aceitar"=>"S"))',
							'imageUrl'=>Yii::app()->request->baseUrl . '/img/certo2.jpg',
						),
					),
				),
				array
				(
					'header'=>'Recusar aula',
					'class'=>'CButtonColumn',
					'template'=>'{Recusar}',
					'visible'=> isset(Yii::app()->user->IndicadorProfessor) ? true : false,
					'buttons'=>array(
						'Recusar'=>array(
							'url'=>'Yii::app()->createUrl("aula/aceitarAula/", array("CodAula"=>$data->CodAula, "Aceitar"=>"N"))',
							'imageUrl'=>Yii::app()->request->baseUrl . '/img/errado2.jpg',
						),
					),
				),
			),
		));
	?>
</div>
</fieldset>