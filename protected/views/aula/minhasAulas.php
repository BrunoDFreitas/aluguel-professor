﻿<fieldset style="margin-top: -48px;">
<legend>Minhas Aulas Agendadas</legend>
<div style='margin-top: -20px;'>
<?php
	$this->widget('zii.widgets.grid.CGridView', array(
		'dataProvider'=>$dp,
		'columns'=>array(
			array(
				'header'=>'Nome da Disciplina',
				'value'=>'$data->Disciplina->NomeDisciplina',
				'htmlOptions'=>array('style'=>'text-align:center;'),
			),
			array(
				'header'=>'Data da aula',
				'name'=>'DataAula',
				'htmlOptions'=>array('style'=>'text-align:center;'),
			),
			array(
				'header'=>'Valor da aula',
				'name'=>'PrecoAula',
				'htmlOptions'=>array('style'=>'text-align:center;'),
			),
			array
			(
				'header'=>'Marcar como realizada',
				'class'=>'CButtonColumn',
				'template'=>'{Realizada}',
				'buttons'=>array(
					'Realizada'=>array(
						'url'=>'Yii::app()->createUrl("aula/aulaRealizada", array("CodAula"=>$data->CodAula, "Realizada"=>"S"))',
						'imageUrl'=>Yii::app()->request->baseUrl . '/img/certo2.jpg',
					),
				),
			),
			array
			(
				'header'=>'Marcar como não-realizada',
				'class'=>'CButtonColumn',
				'template'=>'{Não realizada}',
				'buttons'=>array('Não realizada'=>array(
						'url'=>'Yii::app()->createUrl("aula/aulaRealizada", array("CodAula"=>$data->CodAula, "Realizada"=>"N"))',
						'imageUrl'=>Yii::app()->request->baseUrl . '/img/errado2.jpg',
					),
				),
			),
		),
	));
?>
</div>
</fieldset>