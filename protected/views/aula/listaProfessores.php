﻿<fieldset style="margin-top: -48px;"><legend>Buscar Professores</legend>
<div class="column medium-3" style="text-align: right;
    border: 1px #cccccc solid;
    background-color: #e5f1f4;
    padding-left: 10px;
    padding-right: 10px;
    margin: 10px;
    border-radius: 7px;"
>
	<?php
		echo CHtml::beginForm(Yii::app()->createAbsoluteUrl("aula/listaProfessores"), 'POST', array());
		
		echo "<br />";
		echo CHtml::label('FILTROS', 'label_filtros', array('style'=>'float: left; font-weight: bold;'));
		echo "<br />";
		
		echo CHtml::label('Disciplina: ', 'label_disciplina');
		echo CHtml::textField('Filtro[disciplina]', '', array('maxlenght'=>20, 'style'=>'margin-bottom: 8px;'));
		echo "<br />";
		
		echo CHtml::label('Preço Mínimo: ', 'label_preco_min');
		echo CHtml::textField('Filtro[precoMin]', '0.00', array('style'=>'margin-bottom: 8px;'));
		echo "<br />";
		
		echo CHtml::label('Preço Máximo: ', 'label_preco_max');
		echo CHtml::textField('Filtro[precoMax]', '999.99', array('style'=>'margin-bottom: 8px;'));
		echo "<br />";
		echo "<br />";
	?>
</div>
<div class="column medium-8" style="height: 155px;">
</div>
<?php
	echo CHtml::submitButton('Filtrar', array('class' => 'btn', 'style'=>'margin-left: 10px;'));
?>
<div>
	<?php
		$this->widget('ext.GroupGridView', array(
			'id' => 'grid1',
			'dataProvider' => $lst_prof,
			'mergeColumns' => array('NomePessoa'),
			//'mergeType'=>'nested',
			'enableSorting' => false,
			'summaryText' => '',
			'columns' => array(
				array(
					'header'=>'Professor',
					'name'=>'NomePessoa',
				),
				array(
					'header'=> 'Nome da Disciplina',
					'name'=>'NomeDisciplina'
				),
				array(
					'header'=> 'Assunto tratado na disciplina',
					'name'=>'AssuntoDisciplina',
				),
				array(
					'header'=> 'Preço (R$)',
					'name'=>'PrecoDisciplina',
				),
				array
				(
					'header'=>'Operações',
					'class'=>'CButtonColumn',
					'template'=>'{view}',
					'buttons'=>array(
						'view'=>array(
							'url'=>'Yii::app()->createUrl("aula/detalharDisciplina/", array("CodDisciplina"=>$data->CodDisciplina))',
						),
					),
				),
			),
		));
	?>
</div>
</fieldset>