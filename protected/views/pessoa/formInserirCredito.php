<fieldset>
<legend>Inserir Créditos</legend>
<?php
	echo CHtml::beginForm(Yii::app()->createAbsoluteUrl("pessoa/inserirCredito"), 'POST', array());
	echo CHtml::label('Saldo disponível: ', 'label_saldo', array());
	echo "<b>" . CHtml::label($saldo, '') . "</b>";
	echo "<br />";
	echo CHtml::label('Quantidade para depositar: ', 'label_deposito', array());
	echo CHtml::textField('Pessoa[Depositar]', "0.00", array());
	echo "<br />";
	echo CHtml::submitButton('Enviar', array('class' => 'btn', 'style'=>'margin-left: 5px;'));
?>
</fieldset>