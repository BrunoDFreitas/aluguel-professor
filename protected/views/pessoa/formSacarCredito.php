<fieldset>
<legend>Sacar Créditos</legend>
<?php
	echo CHtml::beginForm(Yii::app()->createAbsoluteUrl("pessoa/sacarCredito"), 'POST', array());
	echo CHtml::label('Saldo disponível: ', 'label_saldo', array());
	echo "<b>" . CHtml::label($saldo, '') . "</b>";
	echo "<br />";
	echo CHtml::label('Quantidade para sacar: ', 'label_sacar', array());
	echo CHtml::textField('Pessoa[Sacar]', "0.00", array());
	echo "<br />";
	echo CHtml::submitButton('Sacar', array('class' => 'btn', 'style'=>'margin-left: 5px;'));
?>
</fieldset>