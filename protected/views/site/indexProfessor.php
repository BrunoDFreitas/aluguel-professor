<fieldset><legend>Bem-vindo</legend>
	<p>Clique <?php echo CHtml::link('aqui', Yii::app()->createAbsoluteUrl("disciplina/minhasDisciplinas")); ?> para visualizar e editar suas disciplinas!</p>
	<p>Para visualizar suas aulas, clique <?php echo CHtml::link('aqui', Yii::app()->createAbsoluteUrl("aula/minhasAulas")); ?>!</p>
</fieldset>