<style>
div.form-login {
    text-align: center;
}
</style>

<fieldset>
	<legend>Login de Funcionário</legend>
	<div class="form-login">
		<?php
			echo CHtml::beginForm(Yii::app()->createAbsoluteUrl("site/loginFuncionario"), 'POST', array());
			?>
			<div class="row">
			<div class="column medium-7" style="text-align: end;">
			<?php
				echo CHtml::label('Usuario: ', 'label_usuario');
				echo CHtml::textField('Funcionario[Usuario]', '', array('maxlength'=>20, 'style'=>'margin-bottom: 8px;'));
				echo "<br />";
				
				echo CHtml::label('Senha: ', 'label_senha');
				echo CHtml::passwordField('Funcionario[Senha]', '', array('maxlength'=>14));
				echo "<br /><br />";
			?>
			</div>
			<div class="column medium-5"></div>
			</div>
			<?php
			
			echo CHtml::submitButton('Entrar', array('class' => 'btn',));
		?>
	</div>
</fieldset>