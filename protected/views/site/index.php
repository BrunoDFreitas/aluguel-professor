﻿<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;
?>

<fieldset style="margin-top: -47px;"><legend>Bem-vindo</legend>
	<p>Para se logar, preencha o seu CPF e senha na página de <?php echo CHtml::link('login', Yii::app()->createAbsoluteUrl("site/novoLogin")); ?>!</p>
	<p>Para se cadastrar acesse a <?php echo CHtml::link('página', Yii::app()->createAbsoluteUrl("pessoa/meuPerfil")); ?> de cadastro!</p>
</fieldset>